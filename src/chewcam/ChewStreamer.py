import sys, os, time
import gi
gi.require_version('Gst','1.0')
from gi.repository import Gst, GLib, GObject
import threading

class ChewStreamer(object):
    settings = {}
    def __init__(self):
        self.loop = GObject.MainLoop()
        GObject.threads_init()
        Gst.init(None)
        #chew (mp3), icecast(mp3), local audio(mp3), local video (flv)
        #self.pipeline = Gst.parse_launch("rpicamsrc bitrate=1000000 do-timestamp=true name=camsrc ! video/x-h264,width=1280,height=720,framerate=24/1,profile=high ! h264parse ! queue ! flvmux name=mux audiotestsrc ! audio/x-raw,rate=44100,channels=2 ! lamemp3enc bitrate=256 ! tee name=audio ! mpegaudioparse ! queue ! mux. mux. ! tee name=video video. ! fakesink audio. ! fakesink")
    def getstatus(self):
        return {}
    def previewstream(self):
        #self.pipeline = Gst.parse_launch("audiotestsrc ! audio/x-raw,rate=44100,channels=2 ! voaacenc bitrate=256000 ! aacparse ! queue ! tee name=audio audio. ! aacparse ! flvmux name=mux rpicamsrc bitrate=1000000 do-timestamp=true name=camsrc ! video/x-h264,width=1280,height=720,framerate=24/1,profile=high ! h264parse ! queue ! mux. mux. ! tee name=video")
        #self.pipeline = Gst.parse_launch("videotestsrc pattern=snow ! xh264enc ! video/x-h264,width=1280,height=720,framerate=24/1,profile=high ! h264parse ! queue ! mux . audiotestsrc ! audio/x-raw,rate=44100,channels=2 ! lamemp3enc bitrate=256 ! tee name=audio ! mpegaudioparse ! queue ! mux. flvmux name=mux ! tee name=video video. ! fakesink audio. ! fakesink")
        #chew, icecast(mp3), local audio(mp3), local video (flv)
        self.pipeline = Gst.parse_launch("rpicamsrc bitrate=1000000 do-timestamp=true name=camsrc ! video/x-h264,width=1280,height=720,framerate=24/1,profile=high ! h264parse ! queue ! flvmux name=mux alsasrc device=plughw:1,0 ! audio/x-raw,rate=44100,channels=2 ! tee name=audio ! queue ! lamemp3enc bitrate=256 ! mpegaudioparse ! mux. mux. ! tcpserversink host=192.168.1.8 port=5000")
        #chew, icecast(aac), local audio(aac), local video (flv)
        #self.pipeline = Gst.parse_launch("rpicamsrc bitrate=1000000 do-timestamp=true name=camsrc ! video/x-h264,width=1280,height=720,framerate=24/1,profile=high ! h264parse ! queue ! flvmux name=mux alsasrc device=plughw:0,0 ! audio/x-raw,rate=44100,channels=2 ! omxaacenc bitrate=256000 ! aacparse ! tee name=audio ! queue ! mux. mux. ! tee name=video ! queue ! valve name=previewvalve drop=0 ! tcpserversink host=192.168.1.8 port=5000 video. ! queue ! valve name=filevalve drop=1 ! filesink location=recording.flv video. ! valve name=chewvalve drop=1 ! rtmpsink name=chewsink audio. ! queue ! valve name=icecastvalve drop=1 ! shout2send description=marrkgrrams genre=dubstep ip=www.clouwdnine.com port=8000 mount=test password=itsallfam streamname=marrkgrrams username=source url=http://www.clouwdnine.com public=true audio. ! filesink=recording.aac")
        #chew, icecast(aac) no recording
        #self.pipeline = Gst.parse_launch("rpicamsrc bitrate=1000000 do-timestamp=true name=camsrc ! video/x-h264,width=1280,height=720,framerate=24/1,profile=high ! h264parse ! queue ! flvmux name=mux alsasrc device=plughw:0,0 ! audio/x-raw,rate=44100,channels=2 ! lamemp3enc bitrate=256 ! id3v2mux ! tee name=audio ! mux. mux. ! queue ! valve name=chewvalve drop=1 ! rtmpsink name=chewsink audio. ! queue ! valve name=icecastvalve drop=1 ! shout2send description=marrkgrrams genre=dubstep ip=www.clouwdnine.com port=8000 mount=test password=itsallfam streamname=marrkgrrams username=source url=http://www.clouwdnine.com public=true")
        #minimal
        #self.pipeline = Gst.parse_launch("rpicamsrc bitrate=1000000 do-timestamp=true name=camsrc ! video/x-h264,width=1280,height=720,framerate=24/1,profile=high ! h264parse ! queue ! flvmux name=mux alsasrc device=plughw:0,0 ! audio/x-raw,rate=44100,channels=2 ! lamemp3enc bitrate=256 ! tee name=audio ! mpegaudioparse ! mux. mux. ! queue ! valve name=chewvalve drop=1 ! rtmpsink name=chewsink audio. ! queue ! valve name=icecastvalve drop=1 ! shout2send description=marrkgrrams genre=dubstep ip=www.clouwdnine.com port=8000 mount=test password=itsallfam streamname=marrkgrrams username=source url=http://www.clouwdnine.com public=true")
        if self.pipeline==None:
            print("No Pipeline")
        self.bus = self.pipeline.get_bus()
        self.bus.add_signal_watch()
        self.bus.connect("message", self.on_message)
        self.play()
    def startstream(self, chewurl):
        self.pause()
        del self.pipeline
        self.pipeline = Gst.parse_launch("rpicamsrc bitrate=1000000 do-timestamp=true name=camsrc ! video/x-h264,width=1280,height=720,framerate=24/1,profile=high ! h264parse ! queue ! flvmux name=mux alsasrc device=plughw:0,0 ! audio/x-raw,rate=44100,channels=2 ! tee name=audio ! queue ! lamemp3enc bitrate=256 ! mpegaudioparse ! mux. mux. ! rtmpsink location=" + chewurl)
        self.bus = self.pipeline.get_bus()
        self.bus.add_signal_watch()
        self.bus.connect("message", self.on_message)
        for k,v in self.settings.iteritems():
            self.update(k, v)
        self.play()
    def on_message(self, bus, message, *args):
        #print(message.keys())
        if message.type == Gst.MessageType.EOS:
            self.pipeline.set_state(Gst.State.NULL)
            self.playmode = False
        elif message.type==Gst.MessageType.ERROR:
            status = message.parse_error()
            print(status)
        elif message.type==Gst.MessageType.WARNING:
            status = message.parse_warning()
            print(status)
        elif message.type==Gst.MessageType.INFO:
            status = message.parse_info()
            print(status)
##    def addsink(self, source, sinktype, parameters):
##        #EG addsink("audio", "filesink", {"location":"recording.mp3"})
##        #print(source + ". ! " + sinktype + " " + " ".join([k + "=" + v for k,v in parameters.iteritems()]))
##        sink = Gst.ElementFactory.make(sinktype)
##        for k,v in parameters.iteritems():
##            sink.set_property(k, v)
##        self.pipeline.add(sink)
##        if source == "audio":
##            parser = Gst.ElementFactory.make("aacparse")
##            self.pipeline.add(parser)
##            self.pipeline.get_by_name(source).link(parser)
##            parser.link(sink)
##        else:
##            self.pipeline.get_by_name(source).link(sink)
##        print("ok")
    def update(self, element, settings):
        #EG update("rpicamsrc",{"awb-mode":"sunlight"})
        #return self.rpicamsrc.get_property(setting)
        self.settings[element] = settings
        element = self.pipeline.get_by_name(element)
        for k,v in settings.iteritems():
            element.set_property(k, str(v))
        return "ok"
    def play(self):
        self.pipeline.set_state(Gst.State.PLAYING)
        try:
            self.loop.run()
        except Exception as e:
            print(e)
    def pause(self):
        self.pipeline.set_state(Gst.State.PAUSED)
    def stop(self):
        self.pipeline.set_state(Gst.State.NULL)

if __name__ == "__main__":
    chewstreamer = ChewStreamer()
    #chewstreamer.addsink("video", "tcpserversink", {"host":"192.168.1.8", "port":5000})
    #chewstreamer.addsink("video", "filesink", {"location":"recording.flv"})
    #chewstreamer.addsink("audio", "filesink", {"location":"recording.mp3", "buffer-size":1000})
    chewstreamer.play()
