from flask import Flask, render_template, request
from ChewStreamer import ChewStreamer
from ChewAPI import ChewAPI
from gi.repository import Gst, GLib, GObject
import threading, json

app = Flask(__name__)

newshow = False
chewstreamer = ""
url = ""

@app.route("/")
def index():
    global mainthread, chewstreamer
    chewstreamer = ChewStreamer()
    mainthread = threading.Thread(target=chewstreamer.previewstream)
    mainthread.start()
    return render_template("interface.html")

@app.route("/previewstream")
def preview_stream():
    global chewstreamer, mainthread
    chewstreamer.previewstream()
    #chewstreamer.playmode = False
    return "killed"

@app.route("/startstream")
def start_stream():
    global chewstreamer, url, mainthread
    chewapi = ChewAPI("clouwdnine@gmail.com","London2014")
    url = chewapi.newshow()
    chewstreamer.startstream(url)
    return "ok"

@app.route("/stopstream")
def stop_stream():
    global chewstreamer, mainthread
    chewstreamer.stop()
    #chewstreamer.playmode = False
    return "killed"

@app.route("/status")
def get_status():
    global chewstreamer, url
    return json.dumps(chewstreamer.getstatus())

@app.route("/update", methods=['GET'])
def update_camera():
    global chewstreamer
    setting = request.args.get('setting', '')
    value = request.args.get('value', '')
    return chewstreamer.update("camsrc", {setting: value})
    
if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0', port=81)

