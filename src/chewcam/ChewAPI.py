import cookielib
import urllib2
import urllib
from bs4 import BeautifulSoup

class ChewAPI:
    cookies = cookielib.LWPCookieJar()
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies))
    def __init__(self, email, password):
        self.opener.addheaders = [('User-agent', 'marrkgrrams/www.marrk.nl')]
        f = self.opener.open("http://chew.tv/login")
        page = BeautifulSoup(f.read())
        values = {}
        for i in page.findAll("input"):
            values[i.get("name")] = i.get("value")
        values["email"] = email
        values["password"] = password
        data = urllib.urlencode(values)
        req = urllib2.Request("http://chew.tv/login", data, { 'User-Agent' : 'marrkgrrams/www.marrk.nl' })
        self.response = self.opener.open(req)
    def newshow(self):
        f = self.opener.open("http://chew.tv/account/show/new")
        page2 = BeautifulSoup(f.read())
        return(page2.select("input#stream-url")[0].get("value") + "/" + page2.select("input#stream-key")[0].get("value"))
    

