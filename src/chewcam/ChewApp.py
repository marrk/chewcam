from flask import Flask, render_template, request
from ChewStreamer import ChewStreamer
from ChewAPI import ChewAPI
from gi.repository import Gst, GLib, GObject
from subprocess import call
import threading, json

app = Flask(__name__)

chewstreamer = ChewStreamer()

@app.route("/")
def index():
    pipe = subprocess.Popen("cat /proc/asound/cards", shell=True, stdout=subprocess.PIPE).stdout
    message = pipe.read()
    flash(message)
    return render_template("start.html")

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0', port=81)

