import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
import os
import time

GObject.threads_init()
Gst.init(None)

class ChewPreviewer:
    def on_message(self, bus, message): 
        #print "%s: %s" % (bus.get_name(), message.type)
        if message.type == Gst.MessageType.ERROR:
            print Gst.Message.parse_error(message)
        elif message.type == Gst.MessageType.STATE_CHANGED:
            print Gst.Message.parse_state_changed(message)
        else:
            print "unknown message"
    def __init__(self):
        self.basepipeline = Gst.parse_launch("rpicamsrc bitrate=1000000 do-timestamp=true name=camsrc ! video/x-h264,width=1280,height=720,framerate=24/1,profile=high ! h264parse ! flvmux name=mux alsasrc device=plughw:1,0 ! audio/x-raw,rate=44100,channels=2 ! lamemp3enc ! mpegaudioparse ! tee name=audio ! queue ! mux. mux. ! tcpserversink host=192.168.1.8 port=5000")
        self.basebus = self.basepipeline.get_bus() 
        self.basebus.add_signal_watch() 
        self.basebus.connect("message", self.on_message) 
        self.basepipeline.set_state(Gst.State.PLAYING) 
    def close(self):
        self.basepipeline.set_state(Gst.State.NULL)
        del self.basepipeline
        return "ok"
if __name__ == "__main__": 
    mainloop = GObject.MainLoop() 
    chewpreviewer = ChewPreviewer()

    try: 
        mainloop.run() 
    except: 
        pass

    print "Finishing" 
    print "Stopping pipeline" 
    chewpreviewer.close()
    del chewpreviewer
