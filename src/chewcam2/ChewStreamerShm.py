import sys, os, time
import gi
gi.require_version('Gst','1.0')
from gi.repository import Gst, GLib, GObject
import threading

class ChewStreamer(object):
    settings = {}
    def __init__(self):
        self.loop = GObject.MainLoop()
        GObject.threads_init()
        Gst.init(None)
        self.pipeline = Gst.parse_launch("rpicamsrc bitrate=1000000 do-timestamp=true name=camsrc ! video/x-h264,width=1280,height=720,framerate=24/1,profile=high ! h264parse ! queue ! flvmux name=mux alsasrc device=plughw:1,0 ! audio/x-raw,rate=44100,channels=2 ! queue ! voaacenc bitrate=192000 ! aacparse ! tee name=audio ! queue ! mux. mux. ! tee name=video ! shmsink socket-path=/tmp/test wait-for-connection=0")
        if self.pipeline==None:
            print("No Pipeline")
        self.bus = self.pipeline.get_bus()
        self.bus.add_signal_watch()
        self.bus.connect("message", self.on_message)
        self.pipeline.set_state(Gst.State.PLAYING)
        try:
            self.loop.run()
        except Exception as e:
            print(e)
##    def previewstream(self):
##        self.pipeline2 = Gst.parse_launch("shmsrc socket-path=/tmp/test is-live=1 ! queue ! tcpserversink host=192.168.1.8 port=5000")
##        if self.pipeline2==None:
##            print("No Pipeline2")
##        self.bus2 = self.pipeline2.get_bus()
##        self.bus2.add_signal_watch()
##        self.bus2.connect("message", self.on_message)
##        self.pipeline2.set_state(Gst.State.PLAYING)
    def on_message(self, bus, message, *args):
        if message.type == Gst.MessageType.EOS:
            self.pipeline.set_state(Gst.State.NULL)
            self.playmode = False
        elif message.type==Gst.MessageType.ERROR:
            status = message.parse_error()
            print(status)
        elif message.type==Gst.MessageType.WARNING:
            status = message.parse_warning()
            print(status)
        elif message.type==Gst.MessageType.INFO:
            status = message.parse_info()
            print(status)

if __name__ == "__main__":
    chewstreamer = ChewStreamer()
#    chewstreamer.update("rpicamsrc",{"preview":"false"})
    #chewstreamer.startpreview()
##    chewstreamer.previewstream()

##    startstream({
##        "rtmp":{
##            "url":"rtmp://test"
##            },
##        "audio":{
##            "filename":"recording.aac"
##            },
##        "video":{
##            "filename":"recording.flv"
##            }
##        }
##    )
                            
    #chewstreamer.previewstream()
