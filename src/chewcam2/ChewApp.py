from flask import Flask, render_template, request, flash
from ChewStreamer import ChewStreamer
from ChewAPI import ChewAPI
from gi.repository import Gst, GLib, GObject
import subprocess
import threading, json
import re

app = Flask(__name__)
app.secret_key = 'some_secret'

#chewstreamer = ChewStreamer()

@app.route("/")
def index():
    pipe = subprocess.Popen("cat /proc/asound/cards", shell=True, stdout=subprocess.PIPE).stdout
    message = pipe.read().split("\n")
    devices = []
    for line in message:
        match = re.search("^ ([0-9]+) \[[^\]]+\]: (.*)", line)
        if match:
            devices.append({"id":match.group(1),"name":match.group(2)})
    flash(devices)
    return render_template("start.html")

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0', port=81)

