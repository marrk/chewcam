import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
import os
import time

GObject.threads_init()
Gst.init(None)

global pipeline2

class ChewStreamer:
    ra,wa = os.pipe()
    rv,wv = os.pipe()
    videocaps = None
    audiocaps = None
    def __init__(self):
        self.basepipeline = Gst.parse_launch("rpicamsrc bitrate=1000000 name=camsrc ! video/x-h264,width=1280,height=720,framerate=24/1,profile=high ! h264parse ! flvmux streamable=true name=mux alsasrc device=plughw:1,0 ! audio/x-raw,rate=44100,channels=2 ! lamemp3enc ! mpegaudioparse ! tee name=audio ! queue ! mux. mux. ! multifdsink socket-path=/tmp/video wait-for-connection=0 shm-size=20000000 name=videosink sync=false audio. ! shmsink socket-path=/tmp/audio wait-for-connection=0 shm-size=20000000 name=audiosink format=BYTE sync=false")
        videosink = self.basepipeline.get_by_name('videosink')
        videosink.get_static_pad('sink').connect('notify::caps', self._onNotifyCaps)
        audiosink = self.basepipeline.get_by_name('audiosink')
        audiosink.get_static_pad('sink').connect('notify::caps', self._onNotifyCaps)
        self.basebus = self.basepipeline.get_bus() 
        self.basebus.add_signal_watch() 
        self.basebus.connect("message", self.on_message) 
        self.basepipeline.set_state(Gst.State.PLAYING) 
    def _getCaps(self, element):
        return self.basepipeline.get_by_name(element).get_static_pad('sink').props.caps
    def _onNotifyCaps(self, pad, unused_property):
        caps = pad.props.caps
        if caps is None or not caps.is_fixed():
            return
        else:
            if caps.to_string().startswith("application"):
                self.videocaps=caps.to_string()
                #self.startpreviewpipeline()
                #self.startlocalrecordingpipeline()
                #self.startchewpipeline("rtmp://stream-eu.chew.tv/live/chew-marrkgrrams-BrLrq")
            else:
                self.audiocaps=caps.to_string()
                #self.starticecastpipeline()
    def on_message(self, bus, message): 
        print "%s: %s" % (bus.get_name(), message.type)
        if message.type == Gst.MessageType.ERROR:
            print Gst.Message.parse_error(message)
        elif message.type == Gst.MessageType.STATE_CHANGED:
            print Gst.Message.parse_state_changed(message)
        else:
            print "unknown message"
    def startchewpipeline(self, url):
        self.videocaps = self._getCaps('videosink').to_string()
        self.chewpipeline = Gst.parse_launch("shmsrc socket-path=/tmp/video is-live=true ! " + self.videocaps + " ! gdpdepay ! rtmpsink location=" + url)
        self.chewbus = self.chewpipeline.get_bus()
        self.chewbus.add_signal_watch()
        self.chewbus.connect("message", self.on_message)
        self.chewpipeline.set_state(Gst.State.PLAYING)
        return "ok"
    def stopchewpipeline(self):
        self.chewpipeline.set_state(Gst.State.NULL)
        del self.chewpipeline
        return "killed"
    def starticecastpipeline(self):
        self.audiocaps = self._getCaps("audiosink").to_string()
        self.icecastpipeline = Gst.parse_launch("shmsrc socket-path=/tmp/audio is-live=true ! " + self.audiocaps + " ! shout2send description=marrkgrrams genre=dubstep ip=www.clouwdnine.com port=8000 mount=test password=itsallfam streamname=marrkgrrams-through-raspi username=source url=http://www.clouwdnine.com public=true")
        self.icecastbus = self.icecastpipeline.get_bus()
        self.icecastbus.add_signal_watch()
        self.icecastbus.connect("message", self.on_message)
        self.icecastpipeline.set_state(Gst.State.PLAYING)
        return "ok"
    def stopicecastpipeline(self):
        self.icecastpipeline.set_state(Gst.State.NULL)
        del self.icecastpipeline
        return "killed"
    def startpreviewpipeline(self):
        self.videocaps = self._getCaps('videosink').to_string()
        print self.videocaps
        self.previewpipeline = Gst.parse_launch("shmsrc socket-path=/tmp/video ! " + self.videocaps + " ! rtpgstdepay ! tcpserversink host=192.168.1.8 port=5000")
        self.previewbus = self.previewpipeline.get_bus()
        self.previewbus.add_signal_watch()
        self.previewbus.connect("message", self.on_message)
        self.previewpipeline.set_state(Gst.State.PLAYING)
        return "started"
    def stoppreviewpipeline(self):
        self.previewpipeline.set_state(Gst.State.NULL)
        del self.previewpipeline
        return "killed"
    def startlocalrecordingpipeline(self):
        print self.videocaps
        self.localrecordingpipeline = Gst.parse_launch("shmsrc socket-path=/tmp/video is-live=true ! " + self.videocaps + " ! filesink location=recording.flv")
        self.localrecordingbus = self.localrecordingpipeline.get_bus()
        self.localrecordingbus.add_signal_watch()
        self.localrecordingbus.connect("message", self.on_message)
        self.localrecordingpipeline.set_state(Gst.State.PLAYING)
        return "started"
    def stoplocalrecordingpipeline(self):
        self.localrecordingpipeline.set_state(Gst.State.NULL)
        del self.localrecordingpipeline
        return "killed"
if __name__ == "__main__": 
    mainloop = GObject.MainLoop() 
    chewstreamer = ChewStreamer()

    try: 
        mainloop.run() 
    except: 
        pass

#    chewstreamer.startpreviewpipeline()

    print "Finishing" 

    #process.send_signal(signal.SIGINT) 
    #process.wait() 
    

    print "Stopping pipeline" 
    #chewstreamer.stoppreviewpipeline()
    try:
        chewstreamer.stopicecastpipeline()
    except:
        pass
    try:
        chewstreamer.stopchewpipeline()                                 
    except:
        pass
    try:
        chewstreamer.stoppreviewpipeline()
    except:
        pass
    chewstreamer.basepipeline.set_state(Gst.State.NULL)
    
    del chewstreamer
