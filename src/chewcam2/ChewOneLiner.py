import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
import os
import time

GObject.threads_init()
Gst.init(None)

class ChewOneLiner:
    def __init__(self):
        url = "rtmp://stream-eu.chew.tv/live/chew-chewcam-28J2E"
        self.basepipeline = Gst.parse_launch("rpicamsrc bitrate=1000000 name=camsrc ! video/x-h264,width=1280,height=720,framerate=24/1,profile=high ! h264parse ! flvmux streamable=true name=mux alsasrc device=plughw:1,0 ! audio/x-raw,rate=44100,channels=2 ! lamemp3enc ! mpegaudioparse ! tee name=audio ! queue ! mux. mux. ! tee name=video ! queue ! rtmpsink location=" + url + " video. ! queue ! filesink location=recording.flv audio. ! queue ! shout2send description=marrkgrrams genre=dubstep ip=www.clouwdnine.com port=8000 mount=test password=itsallfam streamname=marrkgrrams-through-raspi username=source url=http://www.clouwdnine.com public=true audio. ! queue ! filesink location=recording.mp3")
        self.basebus = self.basepipeline.get_bus() 
        self.basebus.add_signal_watch() 
        self.basebus.connect("message", self.on_message) 
        self.basepipeline.set_state(Gst.State.PLAYING) 
    def on_message(self, bus, message): 
        print "%s: %s" % (bus.get_name(), message.type)
        if message.type == Gst.MessageType.ERROR:
            print Gst.Message.parse_error(message)
        elif message.type == Gst.MessageType.STATE_CHANGED:
            print Gst.Message.parse_state_changed(message)
        else:
            print "unknown message"

if __name__ == "__main__": 
    mainloop = GObject.MainLoop() 
    chewoneliner = ChewOneLiner()

    try: 
        mainloop.run() 
    except: 
        pass

#    chewstreamer.startpreviewpipeline()

    print "Finishing" 

    #process.send_signal(signal.SIGINT) 
    #process.wait() 
    

    print "Stopping pipeline" 
    chewoneliner.basepipeline.set_state(Gst.State.NULL)
 
    del chewoneliner
